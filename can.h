#ifndef CAN_H
#define CAN_H
#include <QThread>
#include <iostream>
#include <QDebug>
#include <QLibrary>
#include <QTimer>
#include <QObject>
#include <QMessageBox>
#include "ECanVci.h"
#include <QSemaphore>
#include "zlgcan.h"

class CanDataBase;
class GCcancls : public QThread   //用户自定义的类
{
    Q_OBJECT
public:
    GCcancls();//构造函数//初始化变量

    void stop();

    bool OpenCANThread(int baud);

    void CloseCANThread();

    void ReceiveCANThread();

    void DataReceive();

    bool TransmitCANThread(CanDataBase *cdb);

     uint TransmitCANThread(ZCAN_Transmit_Data* pTransmit, UINT len);
public:

    int devtype=4;//设备类型号//
    int devind=0; //设备索引号
    int res;    //保留参数，通常为0
    int canind=1; //第几路can
    int reftype;//参数类型
    bool musbcanstart=false; //是否打开设备
    bool  IsfindandOpenFlag=false;


 QSemaphore sem;

private:

    CanDataBase *currentReadMsg=nullptr;
    CanDataBase *currentSendMsg=nullptr;
    volatile bool stopped=false;
    int current_CTR=0x22;
    bool TransmitCANThread(unsigned int id, unsigned char *ch,bool EXT,bool RTR, unsigned int len,int sendtype,CanDataBase *cdb);

signals:

    void DataUpdata(CanDataBase *data);
    void my_signal_CHN0(QString str);//信号函数
    void my_signal_CHN1(QString str);//信号函数

    void UDS_RevData_Sig(CanDataBase *cdb);
     void Debug_MSG(QStringList strlist);
};

#endif // CAN_H
