#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMainWindow>
#include <QTimer>
#include <qpushbutton.h>
#include <QTextBrowser>
#include <QLineEdit>
#include <qcombobox.h>
#include <QProgressBar>
#include <qtoolbox.h>

#include "can.h"
#include "Task.h"
#include "zuds.h"
#include "cansetmodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    enum CANtype {
        ZLG = 0,
        GC=1
    };

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    uint32 transmit(const ZUDS_FRAME *frame, uint32 count);
private:
    Ui::MainWindow *ui;

    QTextBrowser *textbrw;

    QString strtemp;

    QThread *candatarevthread;

    CanSetModel *canset;
    int barmax=0;
    GCcancls *MyCANControlThread=nullptr;
    QThread *candatarevthread_GC;
    MainWindow::CANtype type=GC;

    ZUDS_HANDLE uds_handle_;

    bool IScanOPen=false;
    QStringList  His_DTC_list;

private slots:

    void RevsysFun(CanDataBase *cdb);

    void showdatafun(CanDataBase *cdb,bool issend=false);

    void CAndebug(QStringList strlist );

    bool TransCAN(CanDataBase *cdb);

    uint TransCAN(ZCAN_Transmit_Data* pTransmit, UINT len);
    // bool TransCAN(CanDataBase *cdb);
    void UDSFrame2ZCAN(ZCAN_Transmit_Data* can, const ZUDS_FRAME* uds, uint32 count);

    //  uint32 STDCALL  OnUDSTransmitFunc(void* ctx, const ZUDS_FRAME* frame, uint32 count);


    void cleartext();



    void Revone_UDS_fun(CanDataBase *cdb);

    void ZCAN2UDSFrame(ZUDS_FRAME* uds, const ZCAN_Receive_Data* can);



    void SessionControl_0x10_fun();//切换会话模式
    void SecurityAccess_0x27_fun();
    void UDS_STATUS_Fail(ZUDS_RESPONSE res);

    void CurrentDTCNumfun();//读当前DTC数量
    void HisDTCNumfun();//读历史DTC数量
    void CurrentDTCListfun();//读当前DTC
    void HisDTCListfun();//读历史DTC
    void supportedDTCListfun();//读受支持的DTC
    void SpecifiedDTCSnapshotfun();//读快照
    void ClearDTCGroupfun();//清除故障

    void UpdateDTClist_SpecifiedDTCSnapshot();

    void setBtnsize(QPushButton *btn,QSize size);
    void setBoxsize(QComboBox *box,QSize size);


void NRC_resp(uint8 sid,uint8 nrc_code);
signals:
    void startcanrevsig();

    void senddata_cansig(CanDataBase *cdb);

    void start_GCcanrevsig();

    void show_Debug(QStringList  list);
private:
    QPushButton *Btn10;
    QPushButton *Btn11;
    QPushButton *Btn27;

    QPushButton *CurrentDTCNumBtn;
    QPushButton *HisDTCNumBtn;
    QPushButton *CurrentDTCListBtn;
    QPushButton *HisDTCListBtn;
    QPushButton *supportedDTCListBtn;
    QPushButton *SpecifiedDTCSnapshotBtn;
    QPushButton *ClearDTCGroupBtn;

    QComboBox *DTClistbox;//查询指定快照时使用
    QComboBox *DTCGroupbox;

    QComboBox *UDSIDbox;//功能寻址或者物理寻址
    QComboBox *Sessionbox;//会话切换
    QComboBox *SecurityAccessbox;//安全等级
    QToolBox *UDS_toolbox;
    QLineEdit *UDSID_edit;//发送的ID显示

};

#endif // MAINWINDOW_H
