#ifndef CANSETMODEL_H
#define CANSETMODEL_H

#include <QObject>

#include "CANlib/ZLGlib/zlgcan.h"
#include "CANlib/ZLGlib/typedef.h"
#include "CANlib/ZLGlib/config.h"
#include "CANlib/ZLGlib/canframe.h"
#include "candatabase.h"
#include <QMutex>
#include <QThread>
#include <QSemaphore>

class CanSetModel : public QObject
{
    Q_OBJECT
public:
    explicit CanSetModel(QObject *parent = nullptr);

    bool isStart=false;

    QSemaphore sem;

    void SetPara(int type,int index,int CHN,int baud);

    bool CanConnect();

    void CanClose();

    bool CanSend(CanDataBase *data);

    uint CanSend(ZCAN_Transmit_Data* pTransmit, UINT len);

    void DataReceive();

signals:

    void DataUpdata(CanDataBase *data);

    void send_Sta(bool Sta);

    void Debug_MSG(QStringList strlist);

    void UDS_RevData_Sig(CanDataBase *cdb);

    //  void startrevthread();

public slots:

private:
    int DeviceType=ZCAN_USBCAN_2E_U;

    int DeveiceIndex=0;

    int Channel_index=0;

    int baud_index=0;

    CHANNEL_HANDLE chHandle;

    DEVICE_HANDLE dhandle;

    IProperty* property=NULL;

    int current_CTR=0;

    CanDataBase *currentReadMsg=nullptr;
    CanDataBase *currentSendMsg=nullptr;

    void AddData(const ZCAN_Receive_Data *data, UINT len);

    void AddData(CanDataBase *data);
};

#endif // CANSETMODEL_H
