#ifndef CCP_FUNCLA_H
#define CCP_FUNCLA_H

#include <QObject>

#include "candatabase.h"
#include <qtimer.h>



typedef struct Cmd_Recordcls
{
    int CTR;
    int CMD;
    QByteArray candata;
    QString name;
};

class CCP_funcla : public QObject
{

    enum TransmissionDirection {
        Send = 0x01,
        Receive = 0x02
    };

    enum CMD_Sta {
        waitsend=0x00,
        waitreply = 0x01,
        already = 0x02
    };
    enum DAQ_Mode {
        stop = 0,
        start = 1,
        prepare=2
    };

    enum Event_Channel_No {
        Task10ms = 0,
        Task100ms = 1,
        Task500ms=2
    };
    Q_OBJECT
public:

    explicit CCP_funcla(QObject *parent = nullptr);

    int DAQ_count=3;

    bool isconncect=false;

    void Start_Tryconnect();

signals:
    void CHA_up(QString name,QList<uint> data );

    void Transfor(CanDataBase* data);

public slots:




    void sysRevcan(CanDataBase *cdb);

    void connect_fun();

    void DAQMesDown_fun();

    void update_CHA_fun();

    void  DN_CHA_fun( int size,uint address,uint data);


private:
    unsigned char CTR_byte=0;
    unsigned char thelast_sendCTR=0;
    QList<bool> GetDAQSize_First_init;
    QList<bool> GetDAQSize_initlist;
    QMap<int ,Cmd_Recordcls> datarecord;
    QTimer *timer;

    bool tempbool=false;
    void timerEvent(QTimerEvent *event);
    QDateTime thelastsendTime;
    QTimer *ACKtimeout_timer;
    int timerId1;
    CanDataBase *thelast_CMD;
    QList<CanDataBase*> WaitsendData;

    bool DAQ_Set_Errflag=false;
    bool replied=false;

    CMD_Sta CMD_0x15_sta=CMD_Sta::waitsend;
    CMD_Sta CMD_0x16_sta=CMD_Sta::waitsend;
    CMD_Sta CMD_0x06_sta=CMD_Sta::waitsend;

    uint CurrentAddress=0;
    int Add_offset=0;

};



#endif // CCP_FUNCLA_H
