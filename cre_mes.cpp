#include "cre_mes.h"
#include "qdebug.h"
CRE_Mes::CRE_Mes(QObject *parent) : QObject(parent)
{

}

CanDataBase* CRE_Mes::CCP_CONNECT_0x01()//建立连接
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::CONNECT;

    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    cdb->transmit_type=1;
    return cdb;
}


CanDataBase* CRE_Mes::CCP_GET_DAQ_SIZE_0x14(int DAQindex)//获取DAQ列表并初始化
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::GET_DAQ_SIZE;

    tempbyte[2]=DAQindex;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::CCP_SET_DAQ_PTR_0x15(int DAQindex,int  ODTindex,int Element_index  )//设置DAQ元素指针
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::SET_DAQ_PTR;

    tempbyte[2]=DAQindex;
    tempbyte[3]=ODTindex;
    tempbyte[4]=Element_index;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::WRITE_DAQ_0x16(int Size,quint32 Address )//写入DAQ元素指针位置
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::WRITE_DAQ;

    tempbyte[2]=Size;
    tempbyte[3]=0;
    tempbyte[4]=(Address>>(8*3))&0xFF;
    tempbyte[5]=(Address>>(8*2))&0xFF;
    tempbyte[6]=(Address>>(8*1))&0xFF;
    tempbyte[7]=(Address>>(8*0))&0xFF;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::START_STOP_0x06 (DAQ_Mode mode,int DAQindex,int  ODT_RowCount,int Event_CHN_No,int frequency)//开始、停止、准备单个DAQ传输列表
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::START_STOP;

    tempbyte[2]=mode;
    tempbyte[3]=DAQindex;
    tempbyte[4]=ODT_RowCount;
    tempbyte[5]=Event_CHN_No;
    tempbyte[6]=(frequency>>8)&0xFF;
    tempbyte[7]=frequency&0xFF;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::START_STOP_0x08(DAQ_Mode mode)//开始、停止所有DAQ列表
{
    if(mode==DAQ_Mode::prepare)
    {
        mode=DAQ_Mode::start;
    }
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::START_STOP_ALL;

    tempbyte[2]=mode;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::SET_MTA_0x02(quint32 Address)//设置MTA地址指针
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::SET_MTA;

    tempbyte[2]=0;
    tempbyte[3]=0;
    tempbyte[4]=(Address>>(8*3))&0xFF;
    tempbyte[5]=(Address>>(8*2))&0xFF;
    tempbyte[6]=(Address>>(8*1))&0xFF;
    tempbyte[7]=(Address>>(8*0))&0xFF;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::DNLOAD_0x03(QByteArray data)//数据下载
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::DNLOAD;

    tempbyte[2]=data.count();
    for (int var =0; var < data.count(); ++var) {
        tempbyte[3+var]=data.at(var)&0xFF;
    }
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::UPLOAD_0x04 (int size)//数据上传
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::UPLOAD;

    tempbyte[2]=size;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::SHORT_UP_0x0F (int size,quint32 Address)//数据上传（带地址）
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::SHORT_UP;

    tempbyte[2]=size;
    tempbyte[3]=0;
    tempbyte[4]=(Address>>(8*3))&0xFF;
    tempbyte[5]=(Address>>(8*2))&0xFF;
    tempbyte[6]=(Address>>(8*1))&0xFF;
    tempbyte[7]=(Address>>(8*0))&0xFF;
    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}

CanDataBase* CRE_Mes::DISCONNECT_0x07 ()//断开连接
{
    CanDataBase *cdb=new CanDataBase();
    QByteArray tempbyte("01234567");
    tempbyte[0]=CCP_namespace::Command_Table::DISCONNECT;

    cdb->SetData(Send_CCP_CANID,tempbyte,8);
    return cdb;
}



bool CRE_Mes::REV_Data_response(CanDataBase *rev,CanDataBase *sed)
{
    if(rev->Id==Rev_CCP_CANID)
    {

        if((rev->_data[0]&0x00FF)==0xFF)
        {
            if((rev->_data[2]&0x00FF)==(sed->_data[1]&0x00FF))
            {
                return true;
            }
        }
    }

    return false;
}
