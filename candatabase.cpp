#include "candatabase.h"
#include "canframe.h"

CanDataBase::CanDataBase()
{

    IsExtendFrame = false;
    IsRemoteFrame = false;
    hextodecmap.insert("0",0);
    hextodecmap.insert("1",1);
    hextodecmap.insert("2",2);
    hextodecmap.insert("3",3);
    hextodecmap.insert("4",4);
    hextodecmap.insert("5",5);
    hextodecmap.insert("6",6);
    hextodecmap.insert("7",7);
    hextodecmap.insert("8",8);
    hextodecmap.insert("9",9);
    hextodecmap.insert("A",10);
    hextodecmap.insert("B",11);
    hextodecmap.insert("C",12);
    hextodecmap.insert("D",13);
    hextodecmap.insert("E",14);
    hextodecmap.insert("F",15);
}

void CanDataBase::SetTime(QDateTime time)
{
    this->BuildTime = time;
}

void CanDataBase::SetTime(quint64 time)
{
     this->ZCAN_data.timestamp=time;
    this->sTime = time / toms;
    this->spantime_decimal_part= time / toms;
    this->spantime_decimal_part= time % (1000*1000);
    yuanshi_spantimestr=QString::number(this->spantime_decimal_part)+"."+ QString("%1").arg(spantime_decimal_part, 6, 10, QLatin1Char('0'));
}

void CanDataBase:: SetDataState(CanDataBase::SendDataState sta)
{
     this->CurrentState = sta;
}

CanDataBase::SendDataState CanDataBase::GetDataState()
{
    return this->CurrentState;
}

CanDataBase::SendDataState CanDataBase::GetDataState(CanDataBase *cdb)
{
    return cdb->CurrentState;
}

void  CanDataBase::SetNeedACK(bool Sta,int Timeout)
{
    NeedWaitACK=Sta;
    timeOut_inter=Timeout;
}

bool CanDataBase::GetNeedACK()
{
    return  NeedWaitACK;
}

void CanDataBase::SetData(uint id,QByteArray data,int len,bool extend,bool remote)
{
    this->Id = GET_ID(id);
    this->_data =data;
    this->IsExtendFrame = extend;
    this->IsRemoteFrame = remote;
    this->Len = len;

    GetStringData();
}

QString CanDataBase::GetStringData()
{
    QString str ;
    QString strtempfor_;
    for(int i=0;i<this->Len;i++)
    {
      strtempfor_= QString("%1 ").arg( _data.at(i)&0xFF,2,16,QLatin1Char('0')).toUpper();

      str +=strtempfor_;
    }

    this->StrData = str;
    return str;
}


quint64 CanDataBase::toquint64fun(CanDataBase *cdb,bool ismoto)
{
    quint64 canframedata_uint64=0;
    QByteArray  byte_zancun;byte_zancun.clear();
    for (int uop=0;uop<8;uop++) {
        QString STRTEMP=cdb->StrData.split(' ').value(uop).toUpper();
        quint8 data_8 =hextodecmap .value(STRTEMP.left(1))*16+hextodecmap.value(STRTEMP.right(1));
        byte_zancun.append(data_8);
    }

    //  QLOG_INFO()<<cdb->StrData<<byte_zancun.toUpper();

    if(!ismoto)
    {
        for (int y=0;y<byte_zancun.count();y++) {
            quint64 temp64=byte_zancun.at(y)&0XFF;
            quint64 temp64_2=0;
            temp64_2=     temp64<<(8*y);
            canframedata_uint64=temp64_2|canframedata_uint64;
        }

    }else {

        for (int y=0;y<cdb->Len;y++) {
            quint64 temp64=canframedata_uint64;
            quint64 temp64_2=0;
            temp64_2=     temp64<<8;
            canframedata_uint64=temp64_2|(cdb->_data.at(y));
        }
    }
    //   QLOG_INFO()<<cdb->StrData<<canframedata_uint64;
    return  canframedata_uint64;
}

quint64 CanDataBase:: toquint64fun(CanDataBase *cdb,bool ismoto,int startbit,int bitlen)
{
    quint64 canframedata_uint64=0;
    QByteArray  byte_zancun;byte_zancun.clear();
    if(StrData.isEmpty())
    {
        byte_zancun.append(_data);
        //   QLOG_INFO()<< __func__<<__LINE__<<byte_zancun<<_data;
    }else {
        for (int uop=0;uop<8;uop++) {
            QString STRTEMP=cdb->StrData.split(' ').value(uop).toUpper();
            quint8 data_8 =hextodecmap .value(STRTEMP.left(1))*16+hextodecmap.value(STRTEMP.right(1));
            byte_zancun.append(data_8);
        }
        //  QLOG_INFO()<< __func__<<__LINE__<<byte_zancun<<StrData;
    }


    //  QLOG_INFO()<<cdb->StrData<<byte_zancun.toUpper();

    if(!ismoto)
    {
        for (int y=0;y<byte_zancun.count();y++) {
            quint64 temp64=byte_zancun.at(y)&0XFF;
            quint64 temp64_2=0;
            temp64_2=     temp64<<(8*y);
            canframedata_uint64=temp64_2|canframedata_uint64;
        }
        //QLOG_INFO()<< __func__<<__LINE__<<"!ismoto";
    }else {

        for (int y=0;y<cdb->Len;y++) {
            quint64 temp64=canframedata_uint64;
            quint64 temp64_2=0;
            temp64_2=     temp64<<8;
            canframedata_uint64=temp64_2|(cdb->_data.at(y));
        }
    }
    //   QLOG_INFO()<<cdb->StrData<<canframedata_uint64;

    canframedata_uint64=   canframedata_uint64>>startbit;
    quint64 lenmax=0,quint64_1=1;

    for (int var = 0; var < bitlen; ++var) {
        lenmax=(lenmax|(quint64_1<<var));
    }
    canframedata_uint64=canframedata_uint64&lenmax;

    return  canframedata_uint64;
}


