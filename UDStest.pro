#-------------------------------------------------
#
# Project created by QtCreator 2023-07-18T10:44:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UDStest
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        Task.cpp \
        can.cpp \
        candatabase.cpp \
        cansetmodel.cpp \
        cre_mes.cpp \
        main.cpp \
        mainwindow.cpp

HEADERS += \
    zuds.h \
        CANlib/ZLGlib/canframe.h \
        CANlib/ZLGlib/config.h \
        CANlib/ZLGlib/typedef.h \
        CANlib/ZLGlib/zlgcan.h \
        CANlib/GClib/ECanVci.h \
        Task.h \
        can.h \
        candatabase.h \
        cansetmodel.h \
        cre_mes.h \
        mainwindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


win32: LIBS += -L$$PWD/CANlib/ZLGlib/ -lzlgcan
INCLUDEPATH += $$PWD/CANlib/ZLGlib
DEPENDPATH += $$PWD/CANlib/ZLGlib

win32: LIBS += -L$$PWD/CANlib/GClib/ -lECanVci
INCLUDEPATH += $$PWD/CANlib/GClib
DEPENDPATH += $$PWD/CANlib/GClib


unix|win32: LIBS += -L$$PWD/windows/x86/Release/ -lzuds
INCLUDEPATH += $$PWD/windows/x86/Release
DEPENDPATH += $$PWD/windows/x86/Release
