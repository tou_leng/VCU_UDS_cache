#include "Task.h"

void* Run(void *p)
{
    if(p==nullptr)
    {
        return nullptr;
    }
    Task *_this=(Task*)p;
    _this->f();
    delete _this;
    return nullptr;
}

Task::Task(std::function<void()>const &f)
{
    this->f=f;
    ::pthread_create(nullptr,nullptr,Run,this);
}

Task::~Task()
{
    qDebug()<<"Task::~Task";
}
