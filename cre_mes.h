#ifndef CRE_MES_H
#define CRE_MES_H

#include <QObject>
#include "candatabase.h"

class CRE_Mes : public QObject
{
    enum TransmissionDirection {
        Send = 0x01,
        Receive = 0x02
    };

    enum CMD_Sta {
        waitsend=0x00,
        waitreply = 0x01,
        already = 0x02
    };
    enum DAQ_Mode {
        stop = 0,
        start = 1,
        prepare=2
    };

    enum Event_Channel_No {
        Task10ms = 0,
        Task100ms = 1,
        Task500ms=2
    };
    Q_OBJECT
public:
    explicit CRE_Mes(QObject *parent = nullptr);

    static     CanDataBase*  CCP_CONNECT_0x01();//建立连接

    static    CanDataBase*  CCP_GET_DAQ_SIZE_0x14(int DAQindex);//获取DAQ列表并初始化

    static     CanDataBase* CCP_SET_DAQ_PTR_0x15(int DAQindex,int  ODTindex,int Element_index  );//设置DAQ元素指针

    static    CanDataBase* WRITE_DAQ_0x16(int Size,quint32 Address );//写入DAQ元素指针位置

    static     CanDataBase* START_STOP_0x06 (DAQ_Mode mode,int DAQindex,int  ODT_RowCount,int Event_CHN_No,int frequency);//开始、停止、准备单个DAQ传输列表

    static    CanDataBase* START_STOP_0x08(DAQ_Mode mode);//开始、停止所有DAQ列表

    static    CanDataBase* SET_MTA_0x02(quint32 Address );//设置MTA地址指针

    static    CanDataBase* DNLOAD_0x03(QByteArray data );//数据下载

    static     CanDataBase* UPLOAD_0x04 (int size);//数据上传

    static   CanDataBase* SHORT_UP_0x0F (int size,quint32 Address);//数据上传（带地址）

    static     CanDataBase* DISCONNECT_0x07 ( );//断开连接

    static bool REV_Data_response(CanDataBase *rev,CanDataBase *sed);
signals:

public slots:
};

#endif // CRE_MES_H
