#ifndef CANDATABASE_H
#define CANDATABASE_H

#include <QObject>
#include <QDateTime>
#include <QMap>
#define toms 1000000.0
#include <QSemaphore>
#include "zlgcan.h"

#define  Send_CCP_CANID 0x6F0
#define  Rev_CCP_CANID 0x6F1

#define  Send_UDS_CANID 0x7F3
#define  Rev_UDS_CANID 0x7FB
#define  Send_UDS_7DF 0x7DF

class CanDataBase
{
public:
    enum SendDataState {
        WaitSend = 0,
        AlreadySend=1,
        WaitDeal=2,
        AlreadyDeal=4
    };

    CanDataBase();

    ZCAN_Receive_Data ZCAN_data;

    QSemaphore *sem=nullptr;

    bool IsCCP_protocol=false;

    QDateTime BuildTime;

    QDateTime SendTime;

    QDateTime RevTime;

    QDateTime sysTime;

    double sTime=0;

    bool ishistoryData=false;

    int transmit_type=0;//0=正常发送，1=单次发送，2=自发自收，3=单次自发自收

    QString yuanshi_spantimestr="";

    int spantime_integer_part;//整数部分

    int spantime_decimal_part;//小数部分

    uint Id;

    QByteArray _data;

    QString StrData;

    int Len;

    bool IsExtendFrame;

    bool IsRemoteFrame;

    int CHN_num;

    int timeOut_inter=50;

    void SetTime(QDateTime time);

    void SetTime(quint64 time);

    void SetDataState(CanDataBase::SendDataState sta);

    CanDataBase::SendDataState GetDataState();

    CanDataBase::SendDataState GetDataState(CanDataBase *cdb);

    void  SetNeedACK(bool Sta,int Timeout=50);

    bool GetNeedACK();

    void SetData(uint id,QByteArray data,int len,bool extend = false,bool remote = false);

    QString GetStringData();

    quint64 toquint64fun(CanDataBase *cdb,bool ismoto);

    quint64 toquint64fun(CanDataBase *cdb,bool ismoto,int startbit,int bitlen);

private:
    QMap<QString,int> hextodecmap;

    CanDataBase::SendDataState CurrentState=SendDataState::WaitSend;

    bool NeedWaitACK=false;

};

namespace CCP_namespace
{
enum Command_Table
{
    CONNECT	=        0x01,
    GET_CCP_VERSION	=       0x1B,
    EXCHANGE_ID	=        0x17,
    GET_SEED	 =        0x12,
    UNLOCK	   =      0x13,
    SET_MTA	   =      0x02,
    DNLOAD	 =        0x03,
    DNLOAD_6	   =      0x23,
    UPLOAD	     =    0x04,
    SHORT_UP	 =        0x0F,
    SELECT_CAL_PAGE	 =        0x11,
    GET_DAQ_SIZE	   =      0x14,
    SET_DAQ_PTR	   =      0x15,
    WRITE_DAQ	  =       0x16,
    START_STOP	  =       0x06,
    DISCONNECT	    =     0x07,
    SET_S_STATUS	   =      0x0C,
    GET_S_STATUS	    =     0x0D,
    BUILD_CHKSUM	   =      0x0E,
    CLEAR_MEMORY	  =       0x10,
    PROGRAM	   =      0x18,
    PROGRAM_6	   =      0x22,
    MOVE	   =      0x19,
    TEST	   =      0x05,
    GET_ACTIVE_CAL_PAGE	   =      0x09,
    START_STOP_ALL	=         0x08,
    DIAG_SERVICE	=         0x20,
    ACTION_SERVICE	    =     0x21

};
}

#endif // CANDATABASE_H
