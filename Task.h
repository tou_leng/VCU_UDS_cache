#ifndef TASK_H
#define TASK_H

#include <functional>
#include <QDebug>
#include <pthread.h>

class Task : public QObject
{
    Q_OBJECT

public:
    Task(std::function<void()> const &f);
    virtual ~Task() override;

    friend void* Run(void* p);

private:
    std::function<void()> f;
};

#endif // TASK_H
