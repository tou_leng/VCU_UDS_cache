#include "ccp_funcla.h"
#include <QTimerEvent>
#include <qdebug.h>

CCP_funcla::CCP_funcla(QObject *parent) : QObject(parent)
{
   // connect(GlobalVariable::getInstance(),&GlobalVariable::revone_canframe,this,&CCP_funcla::sysRevcan);
    timer =  new QTimer(this);
    timer->setInterval(50);
    connect(timer,&QTimer::timeout,this,&CCP_funcla::connect_fun);
    timerId1 = startTimer(2);
    for (int var = 0; var < DAQ_count; ++var) {
        GetDAQSize_First_init.append(false);
        GetDAQSize_initlist.append(false);
    }
//    connect(GlobalVariable::getInstance(),&GlobalVariable::DAQ_DataSetsig,this,&CCP_funcla::DAQMesDown_fun);
    ACKtimeout_timer=new QTimer();
}



void CCP_funcla::connect_fun()
{

    CanDataBase *data=CCP_CONNECT_0x01();

    Cmd_Recordcls record_temp;
    record_temp.CTR=data->_data[1];
    record_temp.CMD=data->_data[0];
    record_temp.candata=data->_data;
    datarecord.insert(record_temp.CTR,record_temp);
    //    qDebug()<<__func__<<__LINE__<<QDateTime::currentDateTime().toString("ss:zzz")<<"add CTR"<<record_temp.CTR;
    thelast_sendCTR=record_temp.CTR;
    emit Transfor(data);
}

void CCP_funcla:: sysRevcan(CanDataBase *cdb)
{
    if(cdb->Id==Rev_CANID)
    {
        timer->start(5000);
        if((cdb->_data[0]&0xFF)==0xFF)
        {
            replied=true;
            unsigned char CTR=cdb->_data[2]&0xFF;
            int data_size_temp= 0;
            QList<uint> Upload_data;
            if(thelast_sendCTR==CTR)
            {
                switch ( datarecord.value(CTR).CMD&0xFF) {
                case CCP_namespace::Command_Table::CONNECT:
                    //       qDebug()<<__func__<<__LINE__<<QDateTime::currentDateTime().toString("ss:zzz")<<QString::number(cdb->_data[2],16).toUpper()<<  WaitsendData.count();

                    isconncect=true;
                    for (int var = 0; var < DAQ_count; ++var) {
                        if(!GetDAQSize_First_init.value(var))
                        {
                            CanDataBase *data=CCP_GET_DAQ_SIZE_0x14(var);
                            WaitsendData.append(data);
                        }
                    }
                    break;

                case CCP_namespace::Command_Table::GET_DAQ_SIZE:
                    qDebug()<<__func__<<__LINE__<<QDateTime::currentDateTime().toString("ss:zzz")<<cdb->StrData;



                    if(!tempbool)
                    {
                        DAQMes  temp;
                        temp.Size=cdb->_data.at(3)&0xFF;
                        temp.firstAddress=cdb->_data.at(4)&0xFF;
                        GlobalVariable::getInstance()->DaqMes.insert(datarecord.value(CTR).candata.at(2),temp);

                        GetDAQSize_First_init.operator[]( datarecord.value(CTR).candata.at(2))=true;
                        GetDAQSize_initlist.operator[]( datarecord.value(CTR).candata.at(2))=true;

                        tempbool=true;
                        for (int var = 0; var < DAQ_count; ++var) {
                            tempbool=tempbool&GetDAQSize_First_init.value(var);
                        }
                        if(tempbool)
                        {

                            qDebug()<<"所有DAQ初始化完成。";
                        }
                    }else {

                    }


                    break;

                case CCP_namespace::Command_Table::SET_DAQ_PTR:
                    CMD_0x15_sta=CMD_Sta::already;
                    break;

                case CCP_namespace::Command_Table::WRITE_DAQ:
                    CMD_0x16_sta=CMD_Sta::already;
                    break;

                case CCP_namespace::Command_Table::START_STOP:
                    CMD_0x06_sta=CMD_Sta::already;
                    break;

                case CCP_namespace::Command_Table::START_STOP_ALL:

                    break;

                case CCP_namespace::Command_Table::SET_MTA:

                    break;

                case CCP_namespace::Command_Table::DNLOAD:

                    break;

                case CCP_namespace::Command_Table::UPLOAD:

                    break;

                case CCP_namespace::Command_Table::SHORT_UP:
                    data_size_temp=         datarecord.value(CTR).candata.at(2);
    if((data_size_temp>0)&&(data_size_temp<=4))
    {
        emit CHA_up( datarecord.value(CTR).name,Upload_data);
    }else if(data_size_temp==5)
    {
        qDebug()<<"未知原因导致数据未正确解析";
    }

                    break;

                case CCP_namespace::Command_Table::DISCONNECT:

                    break;


                }

            }else if (  thelast_sendCTR!=CTR) {

                //                switch ( datarecord.value(CTR).CMD&0xFF) {
                //                case CCP_namespace::Command_Table::CONNECT:
                //                    qDebug()<<__LINE__<<QDateTime::currentDateTime().toString("ss:zzz")<<QString::number(cdb->_data[2],16).toUpper()<<  WaitsendData.count();
                //                    timer->setInterval(5000);
                //                    timer->start();
                //                    isconncect=true;
                //                    for (int var = 0; var < DAQ_count; ++var) {
                //                        if(!GetDAQSize_First_init.value(var))
                //                        {
                //                            CanDataBase *data=CCP_GET_DAQ_SIZE_0x14(var);
                //                            WaitsendData.append(data);
                //                        }
                //                    }

                //                    break;
                //                default:break;
                //                }
            }
        }
        else  if((cdb->_data[0]&0xFF)==0xFE){//错误发生

        }
    }
}

void CCP_funcla::Start_Tryconnect()
{

    isconncect=false;
    if(timer->isActive())
    {
        return;    }
    timer->start();
}

void CCP_funcla::timerEvent(QTimerEvent *event)
{
    if(!isconncect){return;}

    if (event->timerId() == timerId1)
    {

        if(!WaitsendData.isEmpty())
        {
            if(!replied)
            {
                return;
            }
            thelast_CMD=WaitsendData.takeFirst();
            unsigned char D0=thelast_CMD->_data[0]&0xFF;
            while((DAQ_Set_Errflag==true)&&((D0 ==0x15)||(D0 ==0x16)||(D0 ==0x06)))
            {
                if(!WaitsendData.isEmpty())
                {
                    thelast_CMD=WaitsendData.takeFirst();
                }else {
                    return ;
                }
            }
            D0=thelast_CMD->_data[0]&0xFF;
            switch (D0) {
            case 0x02:CurrentAddress=(thelast_CMD->_data[4]<<24)|(thelast_CMD->_data[5]<<16)|(thelast_CMD->_data[6]<<8)|(thelast_CMD->_data[7]);
            case 0x15:  CMD_0x15_sta=CMD_Sta::waitreply;      break;
            case 0x16:CMD_0x16_sta=CMD_Sta::waitreply; break;
            case 0x06:CMD_0x06_sta=CMD_Sta::waitreply; break;
            default:break;
            }

      emit      Transfor(thelast_CMD);

            Cmd_Recordcls record_temp;
            record_temp.CTR=thelast_CMD->_data[1];
            record_temp.CMD=thelast_CMD->_data[0];
            record_temp.candata=thelast_CMD->_data;
            datarecord.insert(record_temp.CTR,record_temp);
            //    qDebug()<<__func__<<__LINE__<<QDateTime::currentDateTime().toString("ss:zzz")<<"add CTR"<<record_temp.CTR<<thelast_CMD->StrData;
            thelast_sendCTR=record_temp.CTR;
            thelastsendTime=QDateTime::currentDateTime();

            if(thelast_CMD->GetNeedACK())
            {
                replied=false;
                ACKtimeout_timer->start(thelast_CMD->timeOut_inter);
            }
            timer->start();
        }
    }

}



void CCP_funcla::DAQMesDown_fun()
{


}

void CCP_funcla:: update_CHA_fun()
{
 QList<quint32 > address_list,datasizelist;




  for (int var = 0; var < address_list.count(); ++var) {

   CanDataBase *data_02=SET_MTA_0x02(address_list.value(var));
   WaitsendData.append(data_02);
   CanDataBase *data_04=UPLOAD_0x04(datasizelist.value(var));
   WaitsendData.append(data_04);
  }
}

void CCP_funcla:: DN_CHA_fun( int size,uint address,uint data)
{

}
